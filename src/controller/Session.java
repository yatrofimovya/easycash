package controller;

import common.*;

class Session {

    private static final int NUMBER_OF_PIN_ATTEMPTS = 3;

    private CardNumber cardNumber;
    private boolean pinWasProvided;
    private ATM atm;
    private int numberOfPinAttempts;
    private SessionStatus sessionStatus;

    Session() {
        this.numberOfPinAttempts = NUMBER_OF_PIN_ATTEMPTS;
        this.sessionStatus = SessionStatus.undefined;
        this.cardNumber = null;
    }

    CardNumber getCardNumber() {
        return this.cardNumber;
    }

    void setCardNumber(CardNumber cardNumber) {
        this.cardNumber = cardNumber;
    }

    void setPinWasProvided(boolean wasProvided) {
        this.pinWasProvided = wasProvided;
    }

    void decrementPinAttempt() {
        --this.numberOfPinAttempts;
    }

    int getRemainingNumberOfAttempts() {
        return this.numberOfPinAttempts;
    }

    SessionStatus getStatus() {
        return this.sessionStatus;
    }

    void setStatus(SessionStatus s) {
        this.sessionStatus = s;
    }

    public ATM getAtm() {
        return atm;
    }

    public void setAtm(ATM atm) {
        this.atm = atm;
    }
}