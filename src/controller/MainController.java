package controller;

import model.MainModel;

import common.*;

public class MainController {

    private MainModel model;
    private Session session;

    private void init() {
        this.session = new Session();
        this.session.setStatus(SessionStatus.waitingForCard);
    }
    public MainController() {
        this.model = new MainModel();
        this.init();
    }

    public InsertionAttemptResult tryToInsertCard(CardNumber cardNumber) {
        InsertionAttemptResult res;

        if(this.model.isATMTaken()) {
            res = new InsertionAttemptResult(InsertionAttemptResult.Status.atmIsTaken);
        } else if(this.model.isCardPresent(cardNumber)) {
            boolean atmWasTaken = this.model.posessATM();
            if(atmWasTaken) {
                this.session.setCardNumber(cardNumber);
                res = new InsertionAttemptResult(InsertionAttemptResult.Status.completed);
            } else {
                res = new InsertionAttemptResult(InsertionAttemptResult.Status.atmIsTaken);
            }
        } else {
            res = new InsertionAttemptResult(InsertionAttemptResult.Status.wrongCard);
        }

        return res;
    }

    public PinAttemptResult tryToUsePin(CardPin cardPin) {
        PinAttemptResult res;

        CardNumber cardNumber = this.session.getCardNumber();

        if(cardNumber == null) {
            res = new PinAttemptResult(false, this.session.getRemainingNumberOfAttempts());
        } else if(this.model.isPinCorrect(cardNumber, cardPin)) {
            this.session.setPinWasProvided(true);
            res = new PinAttemptResult(true, this.session.getRemainingNumberOfAttempts());
        } else {
            this.session.decrementPinAttempt();
            if(this.session.getRemainingNumberOfAttempts() == 0) {
                this.session.setStatus(SessionStatus.zeroPinAttempts);
            }
            res = new PinAttemptResult(false, this.session.getRemainingNumberOfAttempts());
        }

        return res;
    }

    public WithdrawAttemptResult tryWithdrawMoney(Money money) {
        CardNumber cardNumber = this.session.getCardNumber();
        OperationWithMonyResult splittedByBanknotes = this.tryToSplitByAvailableBanknotes(money);

        if(splittedByBanknotes.isDone()) {
            return this.model.tryWithdrawMoney(cardNumber, splittedByBanknotes);
        } else {
            WithdrawAttemptResult res = new WithdrawAttemptResult(
                WithdrawAttemptResult.Status.notEnoughBanknotes,
                splittedByBanknotes
            );
            return res;
        }
    }
    private OperationWithMonyResult tryToSplitByAvailableBanknotes(Money money) {
        OperationWithMonyResult res = new OperationWithMonyResult();

        int actualAmount = money.getAmount();
        int splitAttemptAmount = 0;

        BanknoteInfo[] sortedDescInfo;
        try {
            sortedDescInfo = this.model.getSortedDescBanknoteInfo();
        } catch(Exception e) {
            res.setIsDone(false);
            return res;
        }

        for(int i = 0; i < sortedDescInfo.length; ++i) {
            BanknoteInfo bi = sortedDescInfo[i];

            int amountToSplit = actualAmount - splitAttemptAmount;

            int bankNoteValue = bi.getBanknoteValue().getAmount();

            int numberOfBanknotesNeeded = amountToSplit / bankNoteValue;
            int numberOfBanknotesPresent = bi.getNumberOfBanknotes();
            if(numberOfBanknotesNeeded > numberOfBanknotesPresent) {
                numberOfBanknotesNeeded = numberOfBanknotesPresent;
            }

            splitAttemptAmount += numberOfBanknotesNeeded * bankNoteValue;

            if(numberOfBanknotesNeeded != 0) {
                res.addBanknoteInfo(new BanknoteInfo(numberOfBanknotesNeeded, new Money(bankNoteValue)));
            }

            if(splitAttemptAmount == actualAmount) {
                res.setIsDone(true);
                break;
            }
        }

        return res;
    }

    public SendMoneyResult trySendMoney(CardNumber to, Money money) {
        CardNumber from = this.session.getCardNumber();
        return this.model.trySendMoney(money, from, to);
    }

    public AccountInfoResult getLoggedAccountInfo() {
        return this.getAccountInfo(this.session.getCardNumber());
    }
    public AccountInfoResult getAccountInfo(CardNumber cardNumber) {
        AccountInfoResult res;

        AccountInfo info = this.model.getAccountInfo(cardNumber);
        if(info == null) {
            res = new AccountInfoResult(AccountInfoResult.Status.wrongCard);
        } else {
            res = new AccountInfoResult(AccountInfoResult.Status.retrieved);
            res.setAccountInfo(info);
        }

        return res;
    }

    public ATMRetrievalResult getAtmInfo() {
        return this.model.getAtmInfo();
    }

    public CardBalanceRetrievalResult getCardBalance() {
        return this.model.getCardBalance(this.session.getCardNumber());
    }
    public SessionStatus getCurrentStatus() {
        return this.session.getStatus();
    }

    public CeaseWorkingResult tryToCeaseWorking() {
        CeaseWorkingResult res;

        boolean ceasedToWork = this.model.freeATM();

        if(ceasedToWork) {
            res = new CeaseWorkingResult(CeaseWorkingResult.Status.completed);
        } else {
            res = new CeaseWorkingResult(CeaseWorkingResult.Status.modelIssues);
        }

        return res;
    }
}
