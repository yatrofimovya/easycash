package common;

public class CardNumber {
    private String cardNumber;

    public CardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String toString() {
        return cardNumber;
    }
}
