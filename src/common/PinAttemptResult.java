package common;

public class PinAttemptResult {
    private boolean isCorrect;
    private int remainingNumberOfAttempts;

    public PinAttemptResult(boolean isCorrect, int remainingNumberOfAttempts) {
        this.isCorrect = isCorrect;
        this.remainingNumberOfAttempts = remainingNumberOfAttempts;
    }

    public boolean isCorrect() {
        return isCorrect;
    }
    public int getRemainingNumberOfAttempts() {
        return this.remainingNumberOfAttempts;
    }
}
