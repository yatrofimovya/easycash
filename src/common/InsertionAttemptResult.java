package common;

public class InsertionAttemptResult {
    private Status status;

    public enum Status {
        completed("Статус: Картка була успішно вставлена."),
        atmIsTaken("Статус: Зараз ATM у використанні, спробуйте пізніше."),
        wrongCard("Статус: Такої картки не існує.");

        private String message;
        Status(String message) {
            this.message = message;
        }

        public String toString() {
            return this.message;
        }
    }

    public InsertionAttemptResult(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return this.status;
    }

    public boolean wasInsertedSuccessfully() {
        if (getStatus() == Status.completed) {
            return true;
        }
        return false;
    }

}
