package common;

public class AccountInfoResult {
    private AccountInfo accountInfo;
    private Status status;

    public enum Status {
        retrieved("Інформація успішно отримана."),
        wrongCard("Такої картки не існує.");

        private String message;
        Status(String message) {
            this.message = message;
        }

        public String toString() {
            return this.message;
        }
    }

    public AccountInfoResult(Status status) {
        this.status = status;
        this.accountInfo = null;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public Status getStatus() {
        return status;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }
}
