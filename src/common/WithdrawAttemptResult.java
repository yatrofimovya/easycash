package common;

public class WithdrawAttemptResult {
    private OperationWithMonyResult splittedBanknotes;
    private Status status;

    public enum Status {
        completed("Гроші були успішно зняті."),
        notEnoughBanknotes("Зараз у банкоматі недостатня кількість купюр."),
        notEnoughMoney("У Вас немає стільки на рахунку."),
        backendError("Система не працює.");

        private String message;
        Status(String message) {
            this.message = message;
        }

        public String toString() {
            return this.message;
        }
    }

    public WithdrawAttemptResult(Status status, OperationWithMonyResult splittedBanknotes) {
        this.status = status;
        this.splittedBanknotes = splittedBanknotes;
    }

    public OperationWithMonyResult getBanknotesSplitting(OperationWithMonyResult splittedBanknotes) {
        return this.splittedBanknotes;
    }

    public OperationWithMonyResult getSplittedBanknotes() {
        return splittedBanknotes;
    }

    public Status getStatus() {
        return status;
    }
}
