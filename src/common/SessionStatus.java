package common;

public enum SessionStatus {
    undefined("undefined"),
    waitingForCard("Статус: Очікування карти"),
    retrievingCardInfo("Статус: Отримання інформації про карту"),
    zeroPinAttempts("Статус: Ви використали всі можливі спроби ввести пін-код");

    private String representation;
    SessionStatus(String representation) {
        this.representation = representation;
    }

    public String toString() {
        return this.representation;
    }
}
