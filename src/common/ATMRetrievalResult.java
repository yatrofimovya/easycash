package common;

public class ATMRetrievalResult {
    private boolean wasSuccessful;
    private ATM atm;

    public ATMRetrievalResult() {
        this.wasSuccessful = false;
        this.atm = null;
    }

    public ATMRetrievalResult(ATM atm) {
        this.wasSuccessful = true;
        this.atm = atm;
    }

    public boolean wasSuccessful() {
        return wasSuccessful;
    }

    public ATM getAtm() {
        return atm;
    }
}
