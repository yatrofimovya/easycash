package common;

public class CardBalanceRetrievalResult {
    private boolean wasSuccessful;
    private Money balance;

    public CardBalanceRetrievalResult() {
        this.wasSuccessful = false;
        this.balance = null;
    }

    public CardBalanceRetrievalResult(Money balance) {
        this.wasSuccessful = true;
        this.balance = balance;
    }

    public boolean wasSuccessful() {
        return this.wasSuccessful;
    }

    public Money getBalance() {
        return this.balance;
    }
}
