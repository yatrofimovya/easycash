package common;

import java.util.LinkedList;
import java.util.List;

public class OperationWithMonyResult {

    public List<BanknoteInfo> amountRepresentation;
    private boolean isDone;

    public OperationWithMonyResult() {
        this.amountRepresentation = new LinkedList<BanknoteInfo>();
        this.isDone = false;
    }

    public void addBanknoteInfo(BanknoteInfo bi) {
        amountRepresentation.add(bi);
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    public boolean isDone() {
        return isDone;
    }

    public List<BanknoteInfo> getAmountRepresentation() {
        return amountRepresentation;
    }

    @Override
    public String toString() {
        return "Номінал купюр: " + amountRepresentation;
    }
}
