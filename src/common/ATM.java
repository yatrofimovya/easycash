package common;

public class ATM {
    private String address;

    public ATM(String address) {
        this.address = address;
    }

    public String getAddress() {
        return this.address;
    }
}
