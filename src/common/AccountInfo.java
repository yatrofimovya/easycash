package common;

public class AccountInfo {
    private String firstName;
    private String lastName;
    private CardNumber cardNumber;

    public AccountInfo(String firstName, String lastName, CardNumber cardNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cardNumber = cardNumber;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public CardNumber getCardNumber() {
        return this.cardNumber;
    }

    @Override
    public String toString() {
        return "Отримувач: " + this.lastName + " " + this.firstName;
    }
}
