package common;

public class SendMoneyResult {
    private Transfer transfer;
    private Status status;

    public enum Status {
        completed("Дякуємо! Гроші було успішно переведено."),
        notEnoughOnCard("Недостатньо коштів для здійснення переводу."),
        databaseError("Виникли помилки під час роботи бази данних. Спробуйте пізніше.");

        private String message;
        Status(String message) {
            this.message = message;
        }

        public String toString() {
            return this.message;
        }
    }

    public Transfer getTransfer() {
        return transfer;
    }
    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
}
