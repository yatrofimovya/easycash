package common;

import java.util.Date;

public class Transfer {
    private CardNumber from;
    private CardNumber to;
    private Money amount;
    private Date date;

    public Transfer(CardNumber from, CardNumber to, Money amount, Date date) {
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.date = date;
    }

    public CardNumber getFrom() {
        return from;
    }
    public CardNumber getTo() {
        return to;
    }
    public Money getAmount() {
        return amount;
    }
}
