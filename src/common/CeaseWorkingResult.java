package common;

public class CeaseWorkingResult {
    private Status status;

    public enum Status {
        completed("Роботу завершено."),
        modelIssues("З технічних причин робота не може бути завершена.");

        private String message;
        Status(String message) {
            this.message = message;
        }

        public String toString() {
            return this.message;
        }
    }

    public CeaseWorkingResult(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return this.status;
    }
}
