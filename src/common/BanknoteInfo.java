package common;

public class BanknoteInfo {

    private int numberOfBanknotes;
    private Money banknoteValue;

    public BanknoteInfo(int numberOfBanknotes, Money banknoteValue) {
        this.numberOfBanknotes = numberOfBanknotes;
        this.banknoteValue = banknoteValue;
    }

    public int getNumberOfBanknotes() {
        return numberOfBanknotes;
    }

    public Money getBanknoteValue() {
        return banknoteValue;
    }

    @Override
    public String toString() {
        return banknoteValue.toString() + "грн" + '\n';
    }
}
