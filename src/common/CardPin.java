package common;

public class CardPin {
    private String cardPin;

    public CardPin(String cardPin) {
        this.cardPin = cardPin;
    }

    public String toString() {
        return cardPin;
    }
}
