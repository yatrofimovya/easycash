package view;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainWindow extends Application {

    static Stage window;

    @Override
    public void start(Stage primaryStage) {
        MainView mv = new MainView();

        window = primaryStage;
        GridPane gridPane = WelcomeWindow.start();

        Scene scene = new Scene(gridPane, 1240,700);
        primaryStage.setTitle("Welcome to ATM");
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    public static void close() {
        window.close();
    }

    public static void main(String[] args) {launch(args);}
}
