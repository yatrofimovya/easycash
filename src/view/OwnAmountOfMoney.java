package view;

import common.*;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import view.components.ModalSuccessWithdrawMoney;
import view.components.NumberPanel;

public class OwnAmountOfMoney {

    public static void newWindow() {
        Stage window = new Stage();

        Button leftUpBtn = new Button(">");
        leftUpBtn.setMinSize(150, 50);
        Button leftDownBtn = new Button("Меню");
        leftDownBtn.setMinSize(150, 50);
        leftDownBtn.setOnAction( e -> {
            window.close();
            MainMenu.newWindow();
        });
        Button rightUpBtn = new Button("<");
        rightUpBtn.setMinSize(150, 50);
        Button rightDownBtn = new Button("Підтвердити");
        rightDownBtn.setMinSize(150, 50);

        Label greetingLabel = new Label("Введіть суму грошей, яку ви хочете зняти");
        greetingLabel.setMaxSize(400, 200);
        greetingLabel.setWrapText(true);

        TextField moneyInput = new TextField();
        moneyInput.setMaxSize(230, 20);


        Label status = new Label("Статус: Введення данних");
        status.setMaxSize(150, 200);
        status.setWrapText(true);

        rightDownBtn.setOnAction(e->
        {
            Money money = new Money(Integer.parseInt(moneyInput.getText().trim()));
            WithdrawAttemptResult withdrawAttemptResult = MainView.instance.withdrawMoney(money);

            MainMenu.newWindow();
            ModalSuccessWithdrawMoney.newWindow(withdrawAttemptResult,
                    withdrawAttemptResult.getSplittedBanknotes());
            window.close();
            ChooseBanknotValue.close();
        });

        GridPane numbers = NumberPanel.getNumberPanel(moneyInput, false);
        Button ok = new Button("Ok");
        numbers.add(ok, 2, 3);
        numbers.setMaxSize(150, 100);

        GridPane gridPane = new GridPane();

        gridPane.setAlignment(Pos.CENTER);

        gridPane.setVgap(25);
        gridPane.setHgap(15);

        gridPane.add(leftUpBtn, 0, 5);
        gridPane.add(leftDownBtn, 0, 6);
        GridPane.setHalignment(greetingLabel, HPos.CENTER);
        gridPane.add(greetingLabel, 1, 1);
        GridPane.setHalignment(moneyInput, HPos.CENTER);
        gridPane.add(moneyInput, 1, 3);
        gridPane.add(rightUpBtn, 2, 5);
        gridPane.add(rightDownBtn, 2, 6);

        GridPane.setHalignment(numbers, HPos.CENTER);
        gridPane.add(numbers, 1, 8);

        GridPane.setHalignment(status, HPos.LEFT);
        gridPane.add(status, 0, 9);

        gridPane.setPadding(new Insets(20));

        Scene scene = new Scene(gridPane, 680,600);
        window.setTitle("ATM");
        window.setScene(scene);

        window.show();
    }
}
