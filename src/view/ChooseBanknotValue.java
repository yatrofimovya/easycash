package view;

import common.*;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import view.components.ModalSuccessWithdrawMoney;
import view.components.NumberPanel;

class ChooseBanknotValue {

    static Stage window;

    static void newWindow() {

        MainWindow.close();
        window = new Stage();

        Button leftUpBtn = new Button("500 грн");
        leftUpBtn.setMinSize(200, 50);
        leftUpBtn.setOnAction(e -> {
            Money money = new Money(500);
            WithdrawAttemptResult withdrawAttemptResult = MainView.instance.withdrawMoney(money);
            MainMenu.newWindow();
            ModalSuccessWithdrawMoney.newWindow(withdrawAttemptResult,
                    withdrawAttemptResult.getSplittedBanknotes());
            window.close();
        });
        Button leftDownBtn = new Button("Ввести вручну");
        leftDownBtn.setMinSize(200, 50);
        leftDownBtn.setOnAction(e -> {
            OwnAmountOfMoney.newWindow();
            window.close();
        });
        Button rightUpBtn = new Button("100 грн");
        rightUpBtn.setMinSize(200, 50);
        rightUpBtn.setOnAction(e -> {
            Money money = new Money(100);
            WithdrawAttemptResult withdrawAttemptResult = MainView.instance.withdrawMoney(money);
            MainMenu.newWindow();
            ModalSuccessWithdrawMoney.newWindow(withdrawAttemptResult,
                    withdrawAttemptResult.getSplittedBanknotes());
            window.close();
            });
        Button rightDownBtn = new Button("200 грн");
        rightDownBtn.setMinSize(200, 50);
        rightDownBtn.setOnAction(e -> {
            Money money = new Money(200);
            WithdrawAttemptResult withdrawAttemptResult = MainView.instance.withdrawMoney(money);
            MainMenu.newWindow();
            ModalSuccessWithdrawMoney.newWindow(withdrawAttemptResult,
                    withdrawAttemptResult.getSplittedBanknotes());
            window.close();
        });

        Label greetingLabel = new Label("Оберіть необхідну суму");
        greetingLabel.setWrapText(true);

        Label status = new Label("Статус: Виконання операції");
        status.setWrapText(true);

        GridPane gridPane = new GridPane();

        gridPane.setAlignment(Pos.CENTER);

        gridPane.setVgap(25);
        gridPane.setHgap(25);

        gridPane.add(leftUpBtn, 0, 4);
        gridPane.add(leftDownBtn, 0, 5);
        GridPane.setHalignment(greetingLabel, HPos.CENTER);
        gridPane.add(greetingLabel, 1, 0);
        gridPane.add(rightUpBtn, 2, 4);
        gridPane.add(rightDownBtn, 2, 5);

        gridPane.add(status, 0, 8);

        gridPane.setPadding(new Insets(20));

        Scene scene = new Scene(gridPane, 700,620);
        window.setTitle("Choose nominal");
        window.setScene(scene);

        window.show();
    }

    static void close() {
        window.close();
    }
}
