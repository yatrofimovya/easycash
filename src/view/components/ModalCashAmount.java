package view.components;

import common.*;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import view.MainView;

import java.util.Date;

public class ModalCashAmount {

    public static void newWindow() {
        Stage window = new Stage();
        window.initModality(Modality.WINDOW_MODAL);
        AccountInfoResult userName = MainView.instance.getUserName();
        Label cashInfo = null;

        ATMRetrievalResult atmRetrievalResult = MainView.instance.getAtmInfo();
        String atmAddress = "not specified";
        if(atmRetrievalResult.wasSuccessful()) {
            atmAddress = atmRetrievalResult.getAtm().getAddress();
        }

        String cardBalance = "not specified";
        CardBalanceRetrievalResult cardBalanceRetrievalResult = MainView.instance.getCardBalance();
        if(cardBalanceRetrievalResult.wasSuccessful()) {
            Money cardBalanceMoney = cardBalanceRetrievalResult.getBalance();
            cardBalance = cardBalanceMoney.toString();
        }

        cashInfo = new Label(new Date().toString() + System.lineSeparator()
                    + atmAddress +
                    System.lineSeparator() +
                    userName.getAccountInfo().getFirstName() + " " + userName.getAccountInfo().getLastName() +
                    System.lineSeparator() + "Баланс: " + cardBalance);

        cashInfo.setWrapText(true);

        GridPane pane = new GridPane();
        GridPane.setHalignment(cashInfo, HPos.CENTER);
        pane.add(cashInfo, 0, 0);

        Image image = new Image("File:img/bill.png");
        ImageView imgView  = new ImageView(image);
        Label lb = new Label("");
        pane.add(lb, 0 , 1);
        pane.add(imgView, 0, 2);

        pane.setPadding(new Insets(20));

        Scene scene = new Scene(pane, 300, 420);
        window.setTitle("Перегляд коштів");
        window.setScene(scene);
        window.show();
    }
}
