package view.components;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class NumberPanel {

    public static GridPane getNumberPanel(TextField passwordInput, boolean defaultBtn) {
        boolean isDefault = defaultBtn;
        Button first = new Button(" 1 ");
        first.setOnAction(e-> passwordInput.appendText("1"));
        first.setDefaultButton(isDefault);
        Button second = new Button(" 2 ");
        second.setOnAction(e->passwordInput.appendText("2"));
        second.setDefaultButton(isDefault);
        Button third = new Button(" 3 ");
        third.setOnAction(e->passwordInput.appendText("3"));
        third.setDefaultButton(isDefault);
        Button fourth = new Button(" 4 ");
        fourth.setOnAction(e->passwordInput.appendText("4"));
        fourth.setDefaultButton(isDefault);
        Button fifth = new Button(" 5 ");
        fifth.setOnAction(e->passwordInput.appendText("5"));
        fifth.setDefaultButton(isDefault);
        Button sixth = new Button(" 6 ");
        sixth.setOnAction(e->passwordInput.appendText("6"));
        sixth.setDefaultButton(isDefault);
        Button seventh = new Button(" 7 ");
        seventh.setOnAction(e->passwordInput.appendText("7"));
        seventh.setDefaultButton(isDefault);
        Button eighth = new Button(" 8 ");
        eighth.setOnAction(e->passwordInput.appendText("8"));
        eighth.setDefaultButton(isDefault);
        Button ninth = new Button(" 9 ");
        ninth.setOnAction(e->passwordInput.appendText("9"));
        ninth.setDefaultButton(isDefault);
        Button del = new Button(" < ");
        del.setDefaultButton(isDefault);
        del.setOnAction(e-> {
            int passwordInputLength = passwordInput.getText().length();
            if (passwordInputLength != 0)
                passwordInput.deleteText(passwordInputLength-1, passwordInputLength);
            else
                passwordInput.setText("");
        });
        Button zero = new Button(" 0 ");
        zero.setOnAction(e->passwordInput.appendText("0"));
        zero.setDefaultButton(isDefault);


        GridPane numbers = new GridPane();
        numbers.add(first,0,0);
        numbers.add(second,1,0);
        numbers.add(third, 2,0);
        numbers.add(fourth, 0,1);
        numbers.add(fifth, 1,1);
        numbers.add(sixth, 2,1);
        numbers.add(seventh, 0,2);
        numbers.add(eighth, 1, 2);
        numbers.add(ninth, 2, 2);
        numbers.add(del, 0, 3);
        numbers.add(zero, 1, 3);

        numbers.setVgap(5);
        numbers.setHgap(5);

        return numbers;
    }


}
