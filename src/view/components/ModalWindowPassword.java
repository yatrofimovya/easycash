package view.components;

import common.CardPin;
import common.PinAttemptResult;
import common.SessionStatus;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import view.MainMenu;
import view.MainView;
import view.MainWindow;
import view.WelcomeWindow;

public class ModalWindowPassword {

    public static void newWindow() {
        Stage window = new Stage();
        window.initModality(Modality.WINDOW_MODAL);

        Label passwordLabel = new Label("Введіть пароль");
        TextField password = new TextField();
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);

        Label status = new Label("Статус: Введення паролю");
        status.setWrapText(true);

        pane.setVgap(25);
        pane.setHgap(15);

        GridPane.setHalignment(passwordLabel, HPos.CENTER);
        pane.add(passwordLabel, 0,0);
        GridPane numbers = NumberPanel.getNumberPanel(password, false);
        Button ok = new Button("Ok");

        GridPane.setHalignment(status, HPos.LEFT);
        pane.add(status, 0,3);

        ok.setOnAction(e->
        {
            status.setText(SessionStatus.retrievingCardInfo.toString());
            CardPin cardPin = new CardPin(password.getText().trim());
            PinAttemptResult pinAttemptResult = MainView.instance.tryToUsePin(cardPin);
            if (pinAttemptResult.isCorrect()) {
                MainMenu.newWindow();
                window.close();
            } else {
               int remainingNumberOfAttempts = pinAttemptResult.getRemainingNumberOfAttempts();

               if(remainingNumberOfAttempts == 0) {
                   MainWindow.close();
                   GridPane gridPane = WelcomeWindow.start();

                   Stage newWindow = new Stage();
                   Scene scene = new Scene(gridPane, 700,650);
                   newWindow.setTitle("Welcome to ATM");
                   newWindow.setScene(scene);
                   newWindow.show();

                   window.close();
               } else {
                   status.setText("Статус: Неправильний пароль. Залишилось " +
                           remainingNumberOfAttempts + " спроб");
               }
            }
        });

        numbers.add(ok, 2, 3);

        GridPane.setHalignment(password, HPos.CENTER);
        pane.add(password, 0,1);

        numbers.setPadding(new Insets(0, 0, 0, 30));
        pane.add(numbers, 0,2);

        pane.setPadding(new Insets(50));

        Scene scene = new Scene(pane, 300, 410);
        window.setTitle("Введення пін-коду");
        window.setScene(scene);
        window.show();
    }
}
