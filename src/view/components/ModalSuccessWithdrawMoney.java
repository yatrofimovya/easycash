package view.components;

import common.OperationWithMonyResult;
import common.WithdrawAttemptResult;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ModalSuccessWithdrawMoney {

    public static void newWindow(WithdrawAttemptResult result, OperationWithMonyResult nomimal) {
        Stage window = new Stage();
        window.initModality(Modality.WINDOW_MODAL);

        StringBuilder nominalRepresentation = new StringBuilder();
        nomimal.getAmountRepresentation().stream().forEach(e -> nominalRepresentation.append(e));
        Label info = null;
        if (result.getStatus() == WithdrawAttemptResult.Status.completed) {
            info = new Label(result.getStatus().toString() + '\n' + "Були видані купюри в номіналі: " +
                    nominalRepresentation);
        } else {
            info = new Label(result.getStatus().toString());
        }
        info.setWrapText(true);

        GridPane pane = new GridPane();
        GridPane.setHalignment(info, HPos.CENTER);
        pane.add(info, 0, 0);

        Image image = new Image("File:img/withdraw-money.jpg");
        ImageView imgView  = new ImageView(image);
        Label lb = new Label("");
        pane.add(lb, 0 , 1);
        pane.add(imgView, 0, 2);

        pane.setPadding(new Insets(20));

        Scene scene = new Scene(pane, 700, 550);
        window.setTitle("Результат операції");
        window.setScene(scene);
        window.show();
    }
}
