package view;

import common.CardNumber;
import common.InsertionAttemptResult;
import common.SessionStatus;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import view.components.*;

public class WelcomeWindow {

    public static GridPane start() {
        Button leftUpBtn = new Button(">");
        leftUpBtn.setMaxSize(150, 80);
        Button leftDownBtn = new Button(">");
        leftDownBtn.setMaxSize(150, 80);
        Button rightUpBtn = new Button("<");
        rightUpBtn.setMaxSize(150, 80);
        Button rightDownBtn = new Button("Розпочати роботу");
        rightDownBtn.setWrapText(true);
        rightDownBtn.setMaxSize(150, 80);

        Label greetingLabel = new Label("Вітаємо! Щоб розпочати роботу введіть номер картки та оберіть " +
                "пункт \"Розпочати роботу\"");
        greetingLabel.setMaxSize(400, 200);
        greetingLabel.setWrapText(true);

        final TextField passwordInput = new TextField();
        passwordInput.setMaxSize(230, 20);


        Label status = new Label(MainView.instance.getCurrentStatus().toString());
        status.setMaxSize(150, 200);
        status.setWrapText(true);

        rightDownBtn.setOnAction(e->
        {
            status.setText(SessionStatus.retrievingCardInfo.toString());
            CardNumber cardNumber = new CardNumber(passwordInput.getText().trim());
            InsertionAttemptResult insertionAttemptResult = MainView.instance
                    .tryToInsertCard(cardNumber);

            if (insertionAttemptResult.wasInsertedSuccessfully()) {
                status.setText(insertionAttemptResult.getStatus().toString());
                ModalWindowPassword.newWindow();
            } else {
                status.setText(insertionAttemptResult.getStatus().toString());
            }
        });

        GridPane numbers = NumberPanel.getNumberPanel(passwordInput, false);
        Button ok = new Button("Ok");
        numbers.add(ok, 2, 3);
        numbers.setMaxSize(150, 100);

        GridPane gridPane = new GridPane();

        gridPane.setVgap(25);
        gridPane.setHgap(15);

        gridPane.add(leftUpBtn, 0, 6);
        gridPane.add(leftDownBtn, 0, 7);
        gridPane.add(greetingLabel, 1, 2);
        GridPane.setHalignment(passwordInput, HPos.CENTER);
        gridPane.add(passwordInput, 1, 4);
        gridPane.add(rightUpBtn, 2, 6);
        gridPane.add(rightDownBtn, 2, 7);

        Image image = new Image("File:img/atm-card.jpg");
        gridPane.add(new ImageView(image), 3, 0, 3, 11);

        GridPane.setHalignment(numbers, HPos.CENTER);
        gridPane.add(numbers, 1, 9);

        GridPane.setHalignment(status, HPos.LEFT);
        gridPane.add(status, 0, 10);

        gridPane.setPadding(new Insets(20));

        return gridPane;
    }
}
