package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import view.components.ModalCashAmount;

public class MainMenu {

    public static void newWindow() {
        MainWindow.close();
        Stage window = new Stage();

        Button leftUpBtn = new Button("Переказати гроші");
        leftUpBtn.setMinSize(200, 100);
        leftUpBtn.setOnAction(e -> {
            TransferWindow.newWindow();
            window.close();
        });

        Label status = new Label("Статус: Вибір типу операції");
        status.setWrapText(true);

        Button leftDownBtn = new Button("Закінчити");
        leftDownBtn.setMinSize(200, 100);
        leftDownBtn.setOnAction(e-> {
            Scene scene = new Scene(
                    WelcomeWindow.start(), 1240,700);
            window.setTitle("Welcome to ATM");
            window.setScene(scene);

            window.show();
        });
        Button rightUpBtn = new Button("Подивитись баланс");
        rightUpBtn.setMinSize(200, 100);
        rightUpBtn.setOnAction(e -> {
            ModalCashAmount.newWindow();
        });
        Button rightDownBtn = new Button("Зняти гроші");
        rightDownBtn.setMinSize(200, 100);
        rightDownBtn.setOnAction(e -> {
            ChooseBanknotValue.newWindow();
            window.close();
        });

        GridPane gridPane = new GridPane();

        gridPane.setAlignment(Pos.CENTER);

        gridPane.setVgap(50);
        gridPane.setHgap(50);

        gridPane.add(leftUpBtn, 0, 1);
        gridPane.add(leftDownBtn, 0, 2);
        gridPane.add(rightUpBtn, 1, 1);
        gridPane.add(rightDownBtn, 1, 2);

        gridPane.add(status, 0, 3);

        gridPane.setPadding(new Insets(20));

        Scene scene = new Scene(gridPane, 700,700);
        window.setTitle("Welcome to ATM");
        window.setScene(scene);

        window.show();
    }
}
