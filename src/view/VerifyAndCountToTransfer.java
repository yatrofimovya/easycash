package view;

import common.AccountInfo;
import common.AccountInfoResult;
import common.Money;
import common.SendMoneyResult;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import view.components.ModalResultTransfer;
import view.components.NumberPanel;

public class VerifyAndCountToTransfer {


    static Stage window;

    public static void newWindow(AccountInfoResult accountInfo) {
        window  = new Stage();
        Button leftUpBtn = new Button(">");
        leftUpBtn.setMinSize(150, 50);
        Button leftDownBtn = new Button("Меню");
        leftDownBtn.setOnAction( e -> {
            window.close();
            MainMenu.newWindow();
        });
        leftDownBtn.setMinSize(150, 50);
        Button rightUpBtn = new Button("<");
        rightUpBtn.setMinSize(150, 50);
        Button rightDownBtn = new Button("Переказати");
        rightDownBtn.setMinSize(150, 50);

        Label greetingLabel = new Label(accountInfo.getAccountInfo().toString());
        greetingLabel.setMinSize(300, 50);
        greetingLabel.setWrapText(true);

        Label toCard = new Label("Сума:");

        TextField cardInput = new TextField();
        cardInput.setMaxSize(230, 20);

        Label status = new Label("Статус: Перевірка данних");
        status.setMaxSize(150, 200);
        status.setWrapText(true);

        GridPane numbers = NumberPanel.getNumberPanel(cardInput, false);
        Button ok = new Button("Ok");
        numbers.add(ok, 2, 3);
        numbers.setMaxSize(150, 100);
        ok.setOnAction(e -> {

        });


        rightDownBtn.setOnAction(e -> {
            SendMoneyResult result = MainView.instance.trySendMoney(accountInfo.getAccountInfo().getCardNumber(),
                    new Money(Integer.parseInt(cardInput.getText())));

            MainMenu.newWindow();
            ModalResultTransfer.newWindow(result);
        });

        GridPane gridPane = new GridPane();

        gridPane.setAlignment(Pos.CENTER);

        gridPane.setVgap(25);
        gridPane.setHgap(15);

        gridPane.add(leftUpBtn, 0, 5);
        gridPane.add(leftDownBtn, 0, 6);
        GridPane.setHalignment(greetingLabel, HPos.CENTER);
        gridPane.add(greetingLabel, 1, 1);
        GridPane.setHalignment(toCard, HPos.CENTER);
        gridPane.add(toCard, 1, 3);
        GridPane.setHalignment(cardInput, HPos.CENTER);
        gridPane.add(cardInput, 1, 4);
        GridPane.setHalignment(rightUpBtn, HPos.RIGHT);
        gridPane.add(rightUpBtn, 2, 5);
        GridPane.setHalignment(rightDownBtn, HPos.RIGHT);
        gridPane.add(rightDownBtn, 2, 6);

        GridPane.setHalignment(numbers, HPos.CENTER);
        gridPane.add(numbers, 1, 9);

        GridPane.setHalignment(status, HPos.LEFT);
        gridPane.add(status, 0, 10);

        gridPane.setPadding(new Insets(20));

        Scene scene = new Scene(gridPane, 690,720);
        window.setTitle("ATM");
        window.setScene(scene);

        window.show();
    }

    public static void close() {
        window.close();
    }
}
