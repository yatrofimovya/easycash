package view;

import common.*;
import controller.MainController;

public class MainView {

    public static MainView instance = new MainView();

    private MainController mainController;

    MainView() {
        this.mainController = new MainController();
    }

    InsertionAttemptResult tryToInsertCard(CardNumber cardNumber) {
        InsertionAttemptResult insertionAttemptResult = mainController.tryToInsertCard(cardNumber);
        return insertionAttemptResult;
    }

    public PinAttemptResult tryToUsePin(CardPin cardPin) {
        PinAttemptResult pinAttemptResult = mainController.tryToUsePin(cardPin);
        return pinAttemptResult;
    }

    public AccountInfoResult getUserName() {
        return mainController.getLoggedAccountInfo();
    }
    AccountInfoResult getAccountInfo(CardNumber cardNumber) {
        return this.mainController.getAccountInfo(cardNumber);
    }

    SessionStatus getCurrentStatus() {
        return mainController.getCurrentStatus();
    }
    public CardBalanceRetrievalResult getCardBalance() {
        return mainController.getCardBalance();
    }

    public ATMRetrievalResult getAtmInfo() {
        return mainController.getAtmInfo();
    }

    WithdrawAttemptResult withdrawMoney(Money money) {
        return this.mainController.tryWithdrawMoney(money);
    }

    SendMoneyResult trySendMoney(CardNumber to, Money money) {
        return this.mainController.trySendMoney(to, money);
    }

    public CeaseWorkingResult tryToCeaseWorking() {
        return this.mainController.tryToCeaseWorking();
    }
}
