package model.dao;
import model.entity.Atm;

import java.util.List;

public interface AtmDao{
    boolean contain(final int id);
    void createATM(final  Atm atm);
    void updateATM(final Atm atm);
    void deleteATM(final int id);
    boolean available(final int id);
    Atm getATM(final int id) throws Exception;
    List<Atm> getAll() throws Exception;
}
