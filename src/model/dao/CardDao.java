package model.dao;

import model.entity.Card;

import java.util.List;

public interface CardDao {
    boolean contain(final int id);
    void createCard(final Card card);
    void updateCard(final Card card);
    void deleteCard(final int id);
    Card getCard(final int id) throws Exception;
    Card getCard(final String card_num)throws Exception;
    boolean isBlocked(final int id) throws Exception;
    List<Card> getAll() throws Exception;
}
