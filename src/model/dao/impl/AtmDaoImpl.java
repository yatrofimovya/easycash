package model.dao.impl;

import model.dao.AtmDao;
import model.entity.Atm;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AtmDaoImpl implements AtmDao{
    
    private static final String DB_URL = "jdbc:mysql://mysql5017.site4now.net/db_a421e8_v4umak";
    //"jdbc:mysql://localhost/atm?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = //"root";
    "a421e8_v4umak";
    private static final String PASS = //"root";
    "atm12345";
    private Connection conn;
    private Statement stmt;
    private ResultSet rs;

    private void connect() {
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Successfully connected!");
        } catch (final Exception ex) {
            System.out.println("Something wrong with connecting to db");
        }
    }

    private void close() {
        try {
            if (stmt != null)
                stmt.close();
            if (rs != null)
                rs.close();
            if (conn != null)
                conn.close();
            System.out.println("Atm conn closed");
        } catch (Exception e) {
            System.err.println(e);
        }
    }


    @Override
    public boolean contain(final int id) {
        connect();
        String query = "SELECT * FROM atm WHERE atm_id = " + id + ';';
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void createATM(final Atm atm) {
        connect();
        String query = "";
        if (atm.getAtm_id() != 0)
            query = "INSERT INTO atm VALUES('" + atm.getAtm_id() + "', '" + atm.getAtm_address() + "', '" +
                    atm.get50() + "', '" + atm.get100() + "', '" + atm.get200() + "', '" + atm.get500() + "')";
        else
            query = "INSERT INTO atm(atm_address, mon50, mon100, mon200, mon500) VALUES('" + atm.getAtm_address() + "', '" +
                    atm.get50() + "', '" + atm.get100() + "', '" + atm.get200() + "', '" + atm.get500() + "')";
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            close();
        } catch (final Exception e) {
            System.out.println("Something wrong while creating a new ATM\n" + e.getMessage() + '\n' + e);
            close();
        }
    }

    @Override
    public void updateATM(final Atm atm) {
        connect();
        String query = "";
        try {
            if (!contain(atm.getAtm_id()))
                throw new Exception("Unable to update unidentified ATM , smth wrong with ID");
            else
                query = "UPDATE atm SET atm_address = '" + atm.getAtm_address() + "', mon50 = '" + atm.get50() +
                        "', mon100 = '" + atm.get100() + "', mon200 = '" + atm.get200() + "', mon500 = '" + atm.get500() +
                        "' WHERE atm_id = '" + atm.getAtm_id() + "'";

            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            close();
        } catch (final Exception e) {
            System.out.println("Something happened while updating ATM\n" + e);
            close();
        }
    }

    @Override
    public void deleteATM(final int id) {
        connect();
        String query = "DELETE FROM atm WHERE atm_id = '" + id + "'";
        try {
            if (!contain(id))
                throw new Exception("Unable to delete unidentified ATM , smth wrong with ID");
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            close();
        } catch (final Exception e) {
            System.out.println("Something wrong while deleting atm #" + id + '\n' + e);
            close();
        }
    }

    @Override
    public boolean available(int id) {
        connect();
        String query = "SELECT available FROM atm WHERE atm_id = '" + id + "'";
        boolean available = false;
        try {
            if (!contain(id))
                throw new Exception("Unable to check availability of unidentified ATM , smth wrong with ID");
            stmt = conn.createStatement();
            rs =stmt.executeQuery(query);
            while (rs.next())
                available = rs.getBoolean("available");
            close();
        } catch (final Exception e) {
            System.out.println("Something wrong while deleting atm #" + id + '\n' + e);
            close();
        }
        return available;
    }

    @Override
    public Atm getATM(final int id) throws Exception {
        connect();
        Atm atm = new Atm("");
        String query = "SELECT * FROM atm WHERE atm_id = '" + id + "'";
        if (!contain(id))
            throw new Exception("Unable to find ATM with such id");
        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            atm = new Atm(rs.getInt("atm_id"), rs.getString("atm_address"),
                    rs.getDouble("mon50"), rs.getDouble("mon100"), rs.getDouble("mon200"),
                    rs.getDouble("mon500"));
        }
        close();
        return atm;
    }

    @Override
    public List<Atm> getAll() throws Exception {
        connect();
        List<Atm> atms = new ArrayList<Atm>();
        String query = "SELECT * FROM atm";
        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
        while (rs.next())
            atms.add(new Atm(rs.getInt(1),rs.getString(2),rs.getDouble(3),
                    rs.getDouble(4),rs.getDouble(5),rs.getDouble(6)));
        close();
        return atms;

    }
}
