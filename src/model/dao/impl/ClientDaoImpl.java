package model.dao.impl;
import model.dao.ClientDao;
import model.entity.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientDaoImpl implements ClientDao {
    private static final String DB_URL ="jdbc:mysql://mysql5017.site4now.net/db_a421e8_v4umak";
    //"jdbc:mysql://localhost/atm?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = //"root";
    "a421e8_v4umak";
    private static final String PASS = //"root";
    "atm12345";
    private Connection conn;
    private Statement stmt;
    private ResultSet rs;

    public void connect() {
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Successfully connected!");
        } catch (final Exception ex) {
            System.out.println("Something wrong with connecting to db");
        }
    }
    private void close() {
        try {
            if (stmt != null)
                stmt.close();
            if (rs != null)
                rs.close();
            if (conn != null)
                conn.close();
            System.out.println("Client conn closed");
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    @Override
    public boolean contain(final int id) {
        connect();
        String query = "SELECT * FROM clients WHERE client_id = '" + id + "'";
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                return true;
            }
        } catch (final Exception ex) {
            System.err.println(ex);
        }
        return false;
    }

    @Override
    public void createClient(final Client client) {
        connect();
        String query = "";
        if (client.getClient_id()!=0)
            query = "INSERT INTO clients VALUES('" + client.getClient_id() + "', '" + client.getFirst_name() + "', '" +
                    client.getLast_name() + "')";
        else
            query = "INSERT INTO clients(first_name, last_name) VALUES('" + client.getFirst_name()+ "', '" +
                    client.getLast_name() + "')";
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            close();
        } catch (final Exception e) {
            System.out.println("Something wrong while creating a new client\n" + e.getMessage() + '\n' + e);
            close();
        }
    }

    @Override
    public void updateClient(final Client client) {
        connect();
        String query = "";
        try {
            if (!contain(client.getClient_id()))
                throw new Exception("Unable to update unidentified client , smth wrong with ID");
            else
                query = "UPDATE clients SET first_name = '" + client.getFirst_name() + "', last_name = '" + client.getLast_name() +
                        "' WHERE client_id = '" + client.getClient_id() + "'";

            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            close();
        } catch (final Exception e) {
            System.out.println("Something happened while updating a client\n" + e);
            close();
        }
    }

    @Override
    public void deleteClient(final int id) {
        connect();
        String query = "DELETE FROM clients WHERE client_id = '" + id + "'";
        try {
            if (!contain(id))
                throw new Exception("Unable to delete unidentified client , smth wrong with ID");
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            close();
        } catch (final Exception e) {
            System.out.println("Something wrong while deleting client #" + id + '\n' + e);
            close();
        }
    }

    @Override
    public Client getClient(final int id) throws Exception {
        connect();
        Client client = new Client();
        String query = "SELECT * FROM clients WHERE client_id = '" + id + "'";
        if (!contain(id))
            throw new Exception("Unable to find client with such id");
        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            client = new Client(rs.getInt("client_id"), rs.getString("first_name"),
                    rs.getString("last_name"));
        }
        close();
        return client;
    }

    @Override
    public List<Client> getAll() throws Exception {
        connect();
        List<Client> clients = new ArrayList<Client>();
        String query = "SELECT * FROM clients ";
        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            clients.add(new Client(rs.getInt("client_id"), rs.getString("first_name"),
                    rs.getString("last_name")));
        }
        close();
        return clients;
    }
}