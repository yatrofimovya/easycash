package model.dao.impl;

import model.dao.CardDao;
import model.entity.Card;
import model.dao.ClientDao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CardDaoImpl implements CardDao {
    private static final String DB_URL = "jdbc:mysql://mysql5017.site4now.net/db_a421e8_v4umak";
    //"jdbc:mysql://localhost/atm?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = //"root";
            "a421e8_v4umak";
    private static final String PASS = //"root";
            "atm12345";
    private Connection conn;
    private Statement stmt;
    private ResultSet rs;

    private void connect() {
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Successfully connected!");
        } catch (final Exception ex) {
            System.out.println("Something wrong with connecting to db");
        }
    }

    private void close() {
        try {
            if (stmt != null)
                stmt.close();
            if (rs != null)
                rs.close();
            if (conn != null)
                conn.close();
            System.out.println("Card conn closed");
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public boolean contain(final String card_num) {
        connect();
        String query = "SELECT * FROM cards WHERE card_num = ?";

        try (PreparedStatement ps = conn.prepareStatement(query)) {

            ps.setString(1, card_num);

            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public boolean contain(final int id) {
        connect();
        try (PreparedStatement ps = conn.prepareStatement("SELECT * FROM cards WHERE card_id = ?")) {

            ps.setInt(1, id);

            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                close();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            close();
        }
        return false;
    }


    @Override
    public void createCard(final Card card) {
        connect();
        String query = "";
        int cl_id = 0;
        ClientDao cd = new ClientDaoImpl();
        if (cd.contain(card.getClient_id()))
            cl_id = card.getClient_id();
        if (card.getCard_id() != 0)
            query = "INSERT INTO cards VALUES('" + card.getCard_id() + "', '" + card.getCard_num() + "', '" +
                    (cl_id == 0 ? null : cl_id) + "', '" + card.getRegistration_date() + "', '" + card.getAmount() + "', '" + card.getPin() + "')";
        else
            query = "INSERT INTO cards(card_num, client_id, registration_date, amount, pin) VALUES('" + card.getCard_num() + "', '" +
                    (cl_id == 0 ? Types.NULL : cl_id) + "', '" + card.getRegistration_date() + "', '" + card.getAmount() + "', '" + card.getPin() + "')";
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            close();
        } catch (final Exception e) {
            System.out.println("Something wrong while creating a new card\n" + e.getMessage() + '\n' + e);
            close();
        }
    }

    @Override
    public void updateCard(final Card card) {
        connect();
        String query = "";
        try {
            if (!contain(card.getCard_id()))
                throw new Exception("Unable to update unidentified card , smth wrong with ID");
            else
                query = "UPDATE cards SET amount = '" + card.getAmount() + "', client_id =' " + card.getClient_id() +
                        "', pin = '" + card.getPin() + "' WHERE card_id = '" + card.getCard_id() + "'";

            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            close();
        } catch (final Exception e) {
            System.out.println("Something happened while updating a card\n" + e);
            close();
        }
    }

    @Override
    public void deleteCard(final int id) {
        connect();
        String query = "DELETE FROM cards WHERE card_id = '" + id + "'";
        try {
            if (!contain(id))
                throw new Exception("Unable to delete unidentified card , smth wrong with ID");
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            close();
        } catch (final Exception e) {
            System.out.println("Something wrong while deleting card #" + id + '\n' + e);
            close();
        }
    }

    @Override
    public Card getCard(final int id) throws Exception {
        connect();
        Card card = new Card();
        String query = "SELECT * FROM cards WHERE card_id = '" + id + "'";
        if (!contain(id))
            throw new Exception("Unable to find card with such id");
        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            card = new Card(rs.getInt("card_id"), rs.getString("card_num"),
                    rs.getInt("client_id"), rs.getDate("registration_date"), rs.getDouble("amount"),
                    rs.getString("pin"));
        }
        close();
        return card;
    }

    @Override
    public Card getCard(final String card_num) throws Exception {
        connect();
        Card card = new Card();
        String query = "SELECT * FROM cards WHERE card_num = '" + card_num + "'";
        if (!contain(card_num))
            throw new Exception("No card with such card number");
        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            card = new Card(rs.getInt("card_id"), rs.getString("card_num"),
                    rs.getInt("client_id"), rs.getDate("registration_date"), rs.getDouble("amount"),
                    rs.getString("pin"));
        }
        close();
        return card;
    }

    @Override
    public boolean isBlocked(int id) throws Exception {
        connect();
        if (!contain(id))
            throw new Exception("No card with such id");
        boolean blocked = true;
        String query = "SELECT blocked FROM cards WHERE card_id = '" + id + "'";
        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
        while (rs.next())
            blocked = rs.getBoolean("blocked");
        close();
        return blocked;
    }

    @Override
    public List<Card> getAll() throws Exception {
        connect();
        List<Card> cards = new ArrayList<Card>();
        String query = "SELECT * FROM cards ";
        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            cards.add(new Card(rs.getInt("card_id"), rs.getString("card_num"),
                    rs.getInt("client_id"), rs.getDate("registration_date"), rs.getDouble("amount"),
                    rs.getString("pin")));
        }
        close();
        return cards;
    }
}
