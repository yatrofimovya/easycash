package model.dao;

import model.entity.Client;

import java.util.List;

public interface ClientDao {
    boolean contain(final int id);
    void createClient(final Client client);
    void updateClient(final Client client);
    void deleteClient(final int id);
    Client getClient(final int id) throws Exception;
    List<Client> getAll() throws Exception;
}
