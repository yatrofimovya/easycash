package model.entity;

import java.sql.Date;

public class Card {
    private int card_id;
    private String card_num;
    private int client_id;
    private Date registration_date;
    private double amount;
    private String pin;
    private boolean blocked;
    private boolean checkPin(final String pin){
        return pin.matches("(\\d{4})");
    }

    private boolean checkCardNum(final String card_num){
        return card_num.matches("(\\d{16})");
    }
    public Card(final int card_id, final String card_num, final int client_id, final Date registration_date,
                final double amount,final String pin) {
        try {
//            if (!checkCardNum(card_num))
//                throw new Exception("Enter right card number(1-9{16})");
            this.card_id = card_id;
            this.card_num = card_num;
            this.client_id = client_id;
            this.registration_date = registration_date;
            this.amount = amount;
            this.pin = checkPin(pin) ? pin : "0000";
            this.blocked = false;
        }
        catch (final Exception exc) {
            System.out.println(exc.getMessage());
        }
    }
    public Card(final int card_id, final String card_num, final Date registration_date){
        this(card_id, card_num,-1, registration_date,0,"0000");
    }

    public Card(){}

    public int getCard_id() {
        return card_id;
    }

    public String getCard_num() {
        return card_num;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(final int client_id) {
        this.client_id = client_id;
    }

    public Date getRegistration_date() {
        return registration_date;
    }


    public double getAmount() {
        return amount;
    }

    public void setAmount(final double amount) {
        this.amount = amount;
    }

    public String getPin() { return pin; }

    public void setPin(final String pin) { if(checkPin(pin))this.pin=pin; }

    public boolean isBlocked(){return blocked;}

    public String toString(){
        return "Card #" + getCard_id() + ", card_num = " + getCard_num() + ", registration_date = " + getRegistration_date().toString()
                + ((getClient_id()==-1)?" without a client": (", client_id = " + getClient_id() + ", amount = " + getAmount() + ", blocked = " + isBlocked()));
    }
}
