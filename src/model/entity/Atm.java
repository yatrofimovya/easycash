package model.entity;

public class Atm {
    private  int atm_id;
    private String atm_address;
    private double banknotes50;
    private double banknotes100;
    private double banknotes200;
    private double banknotes500;
    private boolean available;
    private double getAtmMoney(){return 50*banknotes50+100*banknotes100+ 200*banknotes200+500*banknotes500;}

    public Atm(final int atm_id, final String atm_address, final double banknotes50,final double banknotes100,
               final double banknotes200,final double banknotes500) {
        this.atm_id = atm_id;
        this.atm_address = atm_address;
        this.banknotes50 = banknotes50; this.banknotes100 = banknotes100;
        this.banknotes200 = banknotes200; this.banknotes500 = banknotes500;
        available = false;
    }
    public Atm(final String atm_address){
        this(0,atm_address,0,0,0,0);
    }

    public int getAtm_id() {
        return atm_id;
    }

    public double get50(){
        return banknotes50;
    }
    public double get100(){
        return banknotes100;
    }
    public double get200(){ return banknotes200; }
    public double get500(){
        return banknotes500;
    }
    public boolean isAvailable(){return available;}

    public void setAvailability(boolean availability){available = availability;}
    public void set50(final double amount){
        banknotes50 = amount;
    }
    public void set100(final double amount){ banknotes100 = amount; }
    public void set200(final double amount){
        banknotes200 = amount;
    }
    public void set500(final double amount){
        banknotes500 = amount;
    }

    public String getAtm_address() {
        return atm_address;
    }

    public void setAtm_address(final String atm_address) {
        this.atm_address = atm_address;
    }

    public double getAtm_money() {
        return getAtmMoney();
    }
    public String toString(){
        return "ATM #" + getAtm_id() + ", address = " + getAtm_address() + ", money = " + getAtm_money() + "availability = " + isAvailable();
    }
}
