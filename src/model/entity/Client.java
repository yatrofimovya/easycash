package model.entity;

public class Client {
    private int client_id;
    private String first_name;
    private String last_name;

    public Client(final int client_id, final String first_name, final String last_name){
        this.client_id = client_id;
        this.first_name = first_name;
        this.last_name = last_name;
    }
    public Client(){};
    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(final String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(final String last_name) {
        this.last_name = last_name;
    }

    public int getClient_id() {
        return client_id;
    }
    public String toString(){
        return "Client #" + getClient_id() + ", f_name = " + getFirst_name() + ", l_name = " + getLast_name();
    }
}
