package model;

import common.*;
import model.entity.Atm;
import model.entity.Card;
import model.entity.Client;
import model.service.AtmService;
import model.service.CardService;
import model.service.ClientService;

import java.util.Date;
import java.util.List;

public class MainModel {

    private CardService cardService;
    private ClientService clientService;
    private AtmService atmService;
    public MainModel() {
        cardService = new CardService();
        clientService = new ClientService();
        atmService = new AtmService();
    }

    public boolean isATMTaken() {
        boolean isATMTaken = !atmService.isAvailable(1);
        return isATMTaken;
    }
    public boolean posessATM() {
        boolean wasAbleToTakePossessionOfATM = false;
        try {
            atmService.posessAtm(1);
            wasAbleToTakePossessionOfATM = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wasAbleToTakePossessionOfATM;
    }
    public boolean freeATM() {
        boolean freedATMSuccessfully = false;
        try {
            atmService.freeUpAtm(1);
            freedATMSuccessfully = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return freedATMSuccessfully;
    }

    public boolean isCardPresent(CardNumber cardNumber) {
        return cardService.isCardInSystem(cardNumber.toString());
    }

    public boolean isPinCorrect(CardNumber cardNumber, CardPin cardPin) {
        boolean res;

        try {
            String pin = cardService.getPin(cardNumber.toString());
            if(pin != null) {
                res = pin.equals(cardPin.toString());
            } else {
                res = false;
            }
        } catch (Exception e) {
            res = false;
        }

        return res;
    }

    public CardBalanceRetrievalResult getCardBalance(CardNumber cardNumber) {
        CardBalanceRetrievalResult res = new CardBalanceRetrievalResult();
        try {
            Money money = new Money((int)cardService.getCashAmount(cardNumber.toString()));
            res = new CardBalanceRetrievalResult(money);
        } catch (Exception e) {}
        return res;
    }

    public ATMRetrievalResult getAtmInfo() {
        ATMRetrievalResult res = new ATMRetrievalResult();

        try {
            Atm atmDB = atmService.getAtm(1);
            ATM atm = new ATM(atmDB.getAtm_address());
            res = new ATMRetrievalResult(atm);
        } catch(Exception e) {}

        return res;
    }

    public SendMoneyResult trySendMoney(Money money, CardNumber from, CardNumber to) {
        SendMoneyResult sendMoneyResult = new SendMoneyResult();

        try {
            Card cardFrom = cardService.getCardInfo(from.toString());
            if(cardFrom.getAmount() < money.getAmount()) {
                sendMoneyResult.setStatus(SendMoneyResult.Status.notEnoughOnCard);
                return sendMoneyResult;
            }

            Card cardTo = cardService.getCardInfo(to.toString());
            cardService.setCashAmount(from.toString(),cardFrom.getAmount()- money.getAmount());
            cardService.setCashAmount(to.toString(),cardTo.getAmount()+ money.getAmount());
            sendMoneyResult.setTransfer(new Transfer(from, to, money, new Date()));
        } catch(Exception e) {
            sendMoneyResult.setStatus(SendMoneyResult.Status.databaseError);
        }

        sendMoneyResult.setStatus(SendMoneyResult.Status.completed);

        return sendMoneyResult;
    }

    public WithdrawAttemptResult tryWithdrawMoney(CardNumber cardNumber, OperationWithMonyResult splittebByBanknotes) {
        //check db that it is possible to withdraw such a sum
        //withdraw if it is possible
        //warn that there are not enough money otherwise
        WithdrawAttemptResult res = null;
        try {
            Card card = cardService.getCardInfo(cardNumber.toString());
            double money = 0;
            List<BanknoteInfo> list = splittebByBanknotes.amountRepresentation;
            for (int i =0; i < list.size();i++)
                money+= (list.get(i).getBanknoteValue().getAmount()*list.get(i).getNumberOfBanknotes());
            if(card.getAmount()<money) {
                res = new WithdrawAttemptResult(
                    WithdrawAttemptResult.Status.notEnoughMoney,
                    splittebByBanknotes
                );
                return res;
            }
            cardService.setCashAmount(cardNumber.toString(),card.getAmount() - money);
        } catch (Exception e) {
            res = new WithdrawAttemptResult(
                WithdrawAttemptResult.Status.backendError,
                splittebByBanknotes
            );
            return res;
        }
        res = new WithdrawAttemptResult(
            WithdrawAttemptResult.Status.completed,
            splittebByBanknotes
        );
        return res;
    }

    public BanknoteInfo[] getSortedDescBanknoteInfo() throws Exception {
        Atm atm = atmService.getAtm(1);
        BanknoteInfo [] banknoteInfos = new BanknoteInfo[4];
        banknoteInfos[0] = new BanknoteInfo((int)atm.get500(), new Money(500));
        banknoteInfos[1] = new BanknoteInfo((int)atm.get200(), new Money(200));
        banknoteInfos[2] = new BanknoteInfo((int)atm.get100(), new Money(100));
        banknoteInfos[3] = new BanknoteInfo((int)atm.get50(),  new Money( 50));
        return banknoteInfos;
    }

    public AccountInfo getAccountInfo(CardNumber cardNumber){
        Client client;
        AccountInfo res = new AccountInfo("FirstName","SecondName", cardNumber);
        try {
            int id = cardService.getCardInfo(cardNumber.toString()).getClient_id();
            client = clientService.getClient(id);
            res = new AccountInfo(client.getFirst_name(), client.getLast_name(), cardNumber);
        } catch (Exception e) {
            return null;
        }
        return res;
    }
}
