package model.service;

import model.dao.AtmDao;
import model.dao.impl.AtmDaoImpl;
import model.entity.Atm;

import java.util.List;

public class AtmService {
    private AtmDao atmDao = new AtmDaoImpl();

    public List<Atm> getAll() throws Exception {
        return atmDao.getAll();
    }

    public Atm getAtm(final int id) throws Exception {
        return atmDao.getATM(id);
    }

    public String getAddress(final int id) throws Exception {
        return atmDao.getATM(id).getAtm_address();
    }

    public void setAddress(final int id, final String address) throws Exception {
        Atm atm = atmDao.getATM(id);
        atm.setAtm_address(address);
        atmDao.updateATM(atm);
    }

    public void addAtm(final Atm atm){
        atmDao.createATM(atm);
    }

    public void deleteAtm(final int id){
        atmDao.deleteATM(id);
    }

    public void set50(final int id, final int mon50) throws Exception {
        Atm atm = atmDao.getATM(id);
        atm.set50(mon50);
        atmDao.updateATM(atm);
    }

    public void set100(final int id, final int mon100) throws Exception {
        Atm atm = atmDao.getATM(id);
        atm.set100(mon100);
        atmDao.updateATM(atm);
    }

    public void set200(final int id, final int mon200) throws Exception {
        Atm atm = atmDao.getATM(id);
        atm.set200(mon200);
        atmDao.updateATM(atm);
    }

    public void set500(final int id, final int mon500) throws Exception {
        Atm atm = atmDao.getATM(id);
        atm.set500(mon500);
        atmDao.updateATM(atm);
    }

    public double get50(final int id) throws Exception {
        return atmDao.getATM(id).get50();
    }

    public double get100(final int id) throws Exception {
        return atmDao.getATM(id).get100();
    }

    public double get200(final int id) throws Exception {
        return atmDao.getATM(id).get200();
    }

    public double get500(final int id) throws Exception {
        return atmDao.getATM(id).get500();
    }

    public boolean isAvailable(final int id){
        return atmDao.available(id);
    }
    public void posessAtm(final int id) throws Exception {
        Atm atm = atmDao.getATM(1);
        atm.setAvailability(false);
        atmDao.updateATM(atm);
    }

    public void freeUpAtm(final int id) throws Exception {
        Atm atm = atmDao.getATM(1);
        atm.setAvailability(true);
        atmDao.updateATM(atm);
    }
}
