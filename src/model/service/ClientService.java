package model.service;

import model.dao.impl.ClientDaoImpl;
import model.entity.Client;

import java.util.List;

public class ClientService {
    private ClientDaoImpl clientDao = new ClientDaoImpl();

    public List<Client> getAll() throws Exception {
        return clientDao.getAll();
    }

    public Client getClient(final int id) throws Exception {
        return clientDao.getClient(id);
    }

    public String getF_name(final int id) throws Exception {
        return clientDao.getClient(id).getFirst_name();
    }

    public String getL_name(final int id) throws Exception {
        return clientDao.getClient(id).getLast_name();
    }

    public void addClient(final Client cl){
        clientDao.createClient(cl);
    }

    public void setF_name(final int id, final String F_name) throws Exception {
        Client cl = clientDao.getClient(id);
        cl.setFirst_name(F_name);
        clientDao.updateClient(cl);
    }

    public void setL_name(final int id, final String L_name) throws Exception {
        Client cl = clientDao.getClient(id);
        cl.setLast_name(L_name);
        clientDao.updateClient(cl);
    }

    public void deleteClient(final int id){
        clientDao.deleteClient(id);
    }
}
