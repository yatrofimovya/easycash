package model.service;

import model.dao.impl.CardDaoImpl;
import model.dao.impl.ClientDaoImpl;
import model.entity.Card;
import model.entity.Client;

import java.util.List;

public class CardService {

    private CardDaoImpl cardDao = new CardDaoImpl();
    private ClientDaoImpl clientDao = new ClientDaoImpl();

    public List<Card> getAll() throws Exception {
        return cardDao.getAll();
    }

    public boolean isCardInSystem(String num) {
        System.out.println(num);
        return cardDao.contain(num);
    }

    public void addCard(final Card card){
        cardDao.createCard(card);
    }

    public void deleteCard(final int id){
        cardDao.deleteCard(id);
    }

    public Card getCard(final int id) throws Exception {
        return cardDao.getCard(id);
    }

    public double getCashAmount(final String card_num) throws Exception {
        return cardDao.getCard(card_num).getAmount();
    }

    public Card getCardInfo(final String card_num) throws Exception {
        return cardDao.getCard(card_num);
    }

    public String getPin(final String card_num) throws Exception {
        return cardDao.getCard(card_num).getPin();
    }

    public void setPin(final String card_num, final String pin) throws Exception {
        Card card = cardDao.getCard(card_num);
        card.setPin(pin);
        cardDao.updateCard(card);
    }
    public void setCashAmount(final String card_num, final double cashAmount) throws Exception {
        Card card = cardDao.getCard(card_num);
        card.setAmount(cashAmount);
        cardDao.updateCard(card);
    }

    public Client getClient(final String card_num) throws Exception {
        return clientDao.getClient(cardDao.getCard(card_num).getClient_id());
    }

    public void setClient(final String card_num, final int client_id) throws Exception {
        Card card = cardDao.getCard(card_num);
        card.setClient_id(client_id);
        cardDao.updateCard(card);
    }

    public boolean isBlocked(final Card card)throws Exception{
        return cardDao.isBlocked(card.getCard_id());
    }


}
